from __future__ import print_function, division


import sys, glob
import numpy as np

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec


lsdat = glob.glob('kmc-*.dat')

dd = []
for fname in lsdat:
    d = np.loadtxt(fname)
    if len(d)<1: continue
    
    dd.append( d[-1,3]/d[-1,1] )
    #plt.plot(d[:,1], d[:,3], label=fname)
    #plt.plot(d[:,0], d[:,1], label=fname)


print( sum(dd) / len(dd))

msds = []
psum = 0.0
for n, msd in enumerate(dd):
    psum += msd
    msds.append( psum / (n+1) )

print( msds)

plt.plot(msds, label='convergence')
plt.legend()
plt.show()
