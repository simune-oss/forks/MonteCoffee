#!/usr/bin/env python

import glob, os

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec

import numpy as np
from numpy import array

from ase.io import read, write
from ase.visualize.plot import plot_atoms
import ase.units


lsd = glob.glob('neb-00000*.dir')
ee_b, ee_r, nn_a, eph_i, eph_f, eph_s = [],[],[],[],[],[]
#if not TextVibrations: 
plt.rc('text', usetex=True)
   
for iconf, dname in enumerate(lsd):
    with open(os.path.join(dname, 'summary.dic'),'r') as inf:
        dct = eval(inf.read())
    ee_b.append(dct['E_barrier'])
    ee_r.append(dct['E_reaction'])
    eph_i.append(dct['e_ph_i'])
    eph_f.append(dct['e_ph_f'])
    eph_s.append(dct['e_ph_s'])

    atoms = read(os.path.join(dname, 'initial.traj'))
    nn_a.append(len(atoms)-121)
    
nn_a = np.array(nn_a, dtype=int)
eph_i = np.array(eph_i)
eph_f = np.array(eph_f)
eph_s = np.array(eph_s)

afi = np.zeros(len(eph_f))
aff = np.zeros(len(eph_f))

for ip, (eb, er, n_ag, eei, eef, ees) in enumerate( zip(ee_b, ee_r, nn_a, eph_i, eph_f, eph_s) ):
    a56 = np.prod(eei.real) / np.prod(ees[1:].real)/ase.units.Hartree*6579.683879634054
    a65 = np.prod(eef.real) / np.prod(ees[1:].real)/ase.units.Hartree*6579.683879634054
    afi[ip], aff[ip] = a56, a65
#    print("{: 6.4f} & {: 6.4f} & {: 6.4f} & {: 6.4f} & {:5d} \\\\".format(eb, er, 
#        a56, a65, n_ag))

plt.rc('text', usetex=True)
#font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 14}
mpl.rc('font', **{'size': 14})
lfs = 18

fig = plt.figure(1, figsize=(8,4))
gs = gridspec.GridSpec(1,2)
ax = fig.add_subplot(gs[0,0])
ax.text(0.06, 0.9, "a)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)
#plt.setp(ax.get_xticklabels()+ax.get_yticklabels(), fontsize=fs)
#plt.setp(ax.get_yticklabels(), rotation=90, ha="right", va='center')
ax.set_xlabel(r"$E_r$ (eV)")
ax.set_ylabel(r"$E_a$ (eV)")
ax.plot(ee_r, ee_b, 'o')

ax = fig.add_subplot(gs[0,1])
ax.text(0.06, 0.9, "b)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)
ax.set_xlabel(r"$A(5\rightarrow 6)$ (THz)")
ax.set_ylabel(r"$E_a$ (eV)")
ax.plot(afi, ee_b, 'o')

# ax = fig.add_subplot(gs[1,0])
# ax.text(0.9, 0.9, "c)", color="black", transform=ax.transAxes, fontsize=lfs)
# ax.minorticks_on()
# ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
# ax.tick_params(which='minor', length=3.0)
# ax.set_xlabel(r"$N_{\textrm{a}}$")
# ax.set_ylabel(r"$E_a$ (eV)")
# ax.plot(nn_a, ee_b, 'o')

# ax = fig.add_subplot(gs[1,1])
# ax.text(0.06, 0.9, "d)", color="black", transform=ax.transAxes, fontsize=lfs)
# ax.minorticks_on()
# ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
# ax.tick_params(which='minor', length=3.0)
# ax.set_xlabel(r"$N_{\textrm{a}}$")
# ax.set_ylabel(r"$E_r$ (eV)")
# ax.yaxis.set_label_coords(-0.1,0.5)
# ax.plot(nn_a, ee_r, 'o')

#fig.subplots_adjust(wspace=0.25)
fig.tight_layout()
fig.savefig('get_data.pdf')
#plt.show()
