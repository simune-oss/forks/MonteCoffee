# Script that tests MC Code
from __future__ import print_function, division

import sys
import numpy as np

from ase.cluster import Octahedron

from user_sites import Site
from user_system import System
from user_kmc import NeighborKMC

T=400.    # Temperature
pCO = 2E3 # CO pressure
pO2 = 1E3 # O2 pressure
a = 4.00  # Lattice Parameter (only for ase.atoms)

# Clear up old output files: does it belong here or to one of the constructors?
np.savetxt("time.txt",[])
np.savetxt("coverages.txt",[])
np.savetxt("stype_ev.txt",[])
np.savetxt("stype_ev_other.txt",[])

# Define the system
atoms = Octahedron("Pt",length=8,cutoff=2,latticeconstant=a)
#atoms.edit()

# Define a site for each atom that is free with no pre-defined neighbors.
CNs = np.zeros(len(atoms)) # CNs: atom -> coordination number
for i, at in enumerate(atoms):
    dp = atoms.get_distances(i, range(len(atoms)))
    CNs[i] = (dp < a/np.sqrt(2.0)+0.01).sum()-1

surface_atom_ids = [i for i in range(len(CNs)) if CNs[i]<12] # Define surface atoms as non-bulk:

site_types = sorted(list(set(CNs) ))
stypes = {}
for i,k in enumerate(site_types): stypes[k] = i

# Create a site for each surface-atom:
sites = []
for i,ind in enumerate(surface_atom_ids):
    sites.append( Site(stype = stypes[CNs[ind]], covered = 0, ind = [ind]) )

print("site_types: ", site_types)
print("stypes: ", stypes)
print("len(sites): ", len(sites))

# Instantiate a particle
p = System(atoms=atoms, sites=sites)

# Tagged the ase.atoms by site-types:
maxtag = len(site_types) 
for i,b in enumerate(atoms):
    b.tag = maxtag if i not in surface_atom_ids else site_types.index(CNs[i])

parameters = {"pCO": pCO,"pO2": pO2,"T": T, "Name": "COOx Simulation", 'tend': 1e-8}

sim = NeighborKMC(system = p, parameters=parameters)

sim.run_kmc()


print('plot results with > python plot.py')
