from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

time = np.loadtxt("time.txt")
covs = np.loadtxt("coverages.txt")

print(covs.shape)
print(covs[0])
print(covs[1])

cov_free = [sum([1 for val in covs[i] if val==0])/float(len(covs[0])) for i in range(len(covs))]
cov_CO = [sum([1 for val in covs[i] if val==1])/float(len(covs[0])) for i in range(len(covs))]
cov_O = [sum([1 for val in covs[i] if val==2])/float(len(covs[0])) for i in range(len(covs))]

plt.plot(time,cov_CO,'k-',lw=2,label="CO")
plt.plot(time,cov_O,'r-',lw=2,label="O")
plt.plot(time,cov_free,'m--',lw=2,label="free")
plt.legend(loc=0,frameon=False,fontsize="small")
plt.xlabel("time (s)")
plt.ylabel("Coverage")

plt.savefig('plot.pdf', format='pdf')
plt.show()

