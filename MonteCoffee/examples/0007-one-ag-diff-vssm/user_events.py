r"""
Module: user_events.py

Module that contains all user defined reaction
events. All user-defined events must be derived
from the parent class EventBase. 

See also
--------
events.py in surface directory.

"""
from MonteCoffee.surface.events import EventSurfaceDiffusion

class AgDiffEvent(EventSurfaceDiffusion):
    r"""
    Ag diffusion event class
    
    The event is Ag* + * -> * + Ag*.
    The event is possible if the site is Ag-covered,
    and the neighbor site is empty.
    The rate comes from transition state theory.
    Performing the event removes a Ag from the site,
    and adds it to the other site.

    """

    def __init__(self, params):
        EventSurfaceDiffusion.__init__(self, params)
        
        
        
