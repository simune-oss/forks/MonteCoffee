from __future__ import print_function

import sys, numpy as np

from MonteCoffee.surface.kmc import NeighborKMCSurface

class NeighborKMC(NeighborKMCSurface):

    def __init__(self, system, parameters={}):
        r"""Constructor for NeighborKMC objects.
            
            Method calls constructor of NeighborKMCSurface. 
    
            Parameters
            ----------
            system : System instance
                A system instance with defined neighborlists.

            parameters : dict
                Parameters used, which are dumped to the log file.
                Example: parameters = 
                {'T':700,'Note':'Test simulation'}

            Returns
            -------
            NeighborKMC instance
        """
        import ase.units
        
        dp = parameters
        NeighborKMCSurface.__init__(self, system=system, parameters=dp)
        self.occurencefile = dp['occurencefile'] if 'occurencefile' in dp else None
                
                
    def run_kmc(self):
        r"""Runs a kmc simulation.

            Returns
            -------
            0 if simulation is finished.

        """
        import ase.io
        from timeit import default_timer as timer
        from MonteCoffee.base.kmc import wt
        
        step = 0                  # Initialize the main step counter

        if self.verbose: 
            print('\nRunning simulation...')

        if self.trajectory is not None:
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='w', atoms=_a).write()

        fmt = "{: 9d} {: 10.4e} {: 7d} {: 10.4e} {: 3d}"
        fmtd ="{: 9d} {: 10.4e} {: 7d} {: 10.4e}\n"

        df = open( self.datafile, 'w')

        sqr_d = self.system.sqr_dist_pbc( np.where(self.system.ij2occ>0) )
        tdiss, nchange, is_diss_prev = 0.0, 0, sqr_d>5

        while step<500000 or nchange<200: # finish if statistics if sufficient
            tstart = self.t
            self.frm_step()
            dt = self.t - tstart

            sqr_d = self.system.sqr_dist_pbc( np.where(self.system.ij2occ>0) )

            is_diss = sqr_d>5
            if is_diss:
                tdiss += dt
                if not is_diss_prev: 
                    nchange += 1
            is_diss_prev = is_diss

            if step % self.LogSteps == 0:
                tperc_diss = tdiss*100/self.t
                df.write( fmtd.format(step, self.t, sqr_d, tperc_diss) )
                if self.verbose:
                    print( fmt.format(step, self.t, sqr_d, tperc_diss, nchange) )

            step+=1

        tperc_diss = tdiss*100/self.t
        df.write( fmtd.format(step, self.t, sqr_d, tperc_diss) )
        df.close()

        if self.verbose:
            print( fmt.format(step, self.t, sqr_d, tperc_diss, nchange) )
       

        np.savetxt(self.occurencefile, self.events[0].ntype2occurence, fmt='%15u', )

        if self.trajectory is not None: #Save final configuration
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='a', atoms=_a).write()

        return 0
