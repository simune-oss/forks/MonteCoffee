from __future__ import print_function, division

import os, sys, glob
import numpy as np

from MonteCoffee.base.config import get_parameters


cfg = get_parameters()
print(cfg)

config = cfg['config']

fs = open('run.sh', 'w')

fs.write('export OMP_NUM_THREADS=1\n')

for T in [800, 900, 1000]:
    for lts in [5,6,7,8,9,10, 20, 30, 50, 80, 120, 180, 260]:
        dname = "t-{:05d}_lts-{:03d}.dir".format( T, lts)
        print(dname)
        fs.write('cd {}\npython -u ../test.py 1>test.out 2>test.err &\ncd - \n'.format(dname))
        try:
            os.stat(dname)
        except:
            os.mkdir(dname)
        config.set('Parameters', 'T', T)
        config.set('Parameters', 'lattsize', lts)
        with open(os.path.join(dname, 'kMC_options.cfg'), 'w') as f:
            config.write(f)

