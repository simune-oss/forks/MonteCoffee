from __future__ import print_function, division

import sys, numpy as np

import ase.build, ase.io, ase.constraints

#from user_sites import Site


from MonteCoffee.base.sites import SiteBase
from MonteCoffee.surface.system import SystemSurface
from MonteCoffee.surface.kmc import NeighborKMCSurface
#from user_kmc import NeighborKMC

atoms = ase.build.fcc100('Pt', (6,5,4)) # Define the system
atoms.set_constraint(ase.constraints.FixAtoms(mask=[a.tag>2 for a in atoms]))
ase.io.write('surface_base.traj', atoms)
surf_atom_ids = [i for i,a in enumerate(atoms) if a.tag==2] # Adsorption occurs in the hollow positions

sites = [] # Create a site for each surface atom
for i, atom_id in enumerate(surf_atom_ids):
    sites.append( SiteBase(stype=1, ind = [atom_id]) )

system = SystemSurface(atoms=atoms, sites=sites, coverage=0.0) # Instantiate the slab
print(system)

prm = {}
sim = NeighborKMCSurface(system=system, parameters=prm)

for event in sim.events:
    event.compute_htst(system)
