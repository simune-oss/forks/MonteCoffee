from __future__ import print_function, division

import sys
import numpy as np

import ase.build, ase.io

from MonteCoffee.base.sites import SiteBase
from MonteCoffee.surface.system import SystemSurface
from MonteCoffee.surface.kmc import NeighborKMCSurface

T = 300. # Temperature

# Define the system
atoms = ase.build.fcc100('Pt', (10,10,2))
ase.io.write('surface_base.traj', atoms)

surf_atom_ids = [i for i,a in enumerate(atoms) if a.tag==2] # Adsorption occurs in the hollow positions

# Create a site for each surface-atom:
sites = []
for i, atom_id in enumerate(surf_atom_ids):
    sites.append( SiteBase(stype=1, ind = [atom_id]) )

# Instantiate a particle
system = SystemSurface(atoms=atoms, sites=sites, coverage=0.02)
print(system)

prm = {"T": T, "trajectory": 'kmc.traj', 'tend': 1e-3}
sim = NeighborKMCSurface(system=system, parameters=prm)
sim.run_kmc()

print('Watch steps  ase gui kmc.traj')
