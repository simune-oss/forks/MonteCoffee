r"""
Module: kmc.py
The kMC simulation base module, which defines the NeighborKMCBase
class and its methods. The methods are used to perform kMC 
simulations with the First reaction method.

"""

from __future__ import print_function
import sys, pickle
from random import randint, uniform
import numpy as np

from timeit import default_timer as timer

if sys.version_info[0]>2:
    import configparser
else:
    import ConfigParser as configparser
    
from ase.io import write

wt = np.zeros(10)

class NeighborKMCBase:
    
    def __init__(self, system, parameters={}):
        r"""Constructor for NeighborKMCBase objects.
            
            Method assigns a system to the simulation,
            stores parameters, and reads in software configuration
            from the separate file kMC_options.cfg. 
            Then it sets the time equal to zero prepares to simulate
            using the first reaction method. 
    
            Parameters
            ----------
            system : System instance
                A system instance with defined neighborlists.

            tend : float
                Defines when the simulation has ended.

            parameters : dict
                Parameters used, which are dumped to the log file.
                Example: parameters = 
                {'pCO':1E2,'T':700,'Note':'Test simulation'}

            Returns
            -------
            NeighborKMCBase instance

        """
        assert hasattr(self, 'events')
        assert len(self.events)>0
        
        self.system = system
        self.parameters = parameters
        self.t = 0.0
        
        # Load software configuration
        defaults = {"SaveSteps": 1000, "LogSteps": 1000, "PicklePrefix": None,
        "SaveCovs": "False", "Verbose": "False", "Nspecies": 2, "Delta": 0.25,
        "Nf": 1000, "Ns": 2000, "Ne": 100, "T": 300}
        config = configparser.RawConfigParser(defaults)
        
        lsconfigs = config.read('kMC_options.cfg')
        if 'T' not in parameters:
            self.T = config.getfloat('Parameters', 'T')
            self.parameters['T'] = self.T
        
        self.SaveSteps = config.getint('Parameters', 'SaveSteps')
        self.LogSteps = config.getint('Parameters', 'LogSteps')
        self.PicklePrefix = config.get('Parameters', 'PicklePrefix')
        self.save_coverages = config.getboolean('Options', 'SaveCovs')
        self.verbose = config.getboolean('Options', 'Verbose')

        if self.verbose:
            print('-'*50,'\n', 'kMC initialized', '\n','-'*50, '\n')
            print('kMC temperature T = {} K'.format(self.parameters['T']))
            print('kMC simulation loading...')
        
        # Variables connected to after analysis.
        self.Nsites = len(self.system.sites) 
        self.Nspecies = config.getint('Parameters', 'Nspecies') # THIS SHOULD BE AUTOMATICALLY DEFINED, not a parameter in the config file
        self.times = []
        self.MCstep = []
        self.covered = [] 
        
        # Initialize event book keeping variables
        evnl = [0]*len(self.events) # WARNING: similar trick with numpy arrays delivers list of pointers pointing to the same array!!!
        self.stype_ev, self.stype_ev_other = {}, {}
        self.Nstypes = list(set([s.stype for s in self.system.sites])) # Number of sites' types (kinds, species)
        for i in self.Nstypes:
            self.stype_ev[i] = list(evnl)
            self.stype_ev_other[i] = list(evnl)


        # Variables connected to temporal acceleration
        # --------------------------------------------------
        self.equilEV = [i for i,e in enumerate(self.events) if e.diffev] # Track equilibrated (diffusion) events, i.e. EVENTS with conserving number of particles???

        # Parameters
        self.delta = config.getfloat('Options','Delta') # reversibility tolerance
        self.Nf = config.getint('Options','Nf') # Avg event observance in superbasins
        self.Ns = config.getint('Options','Ns') # update the barriers every Ns step.
        self.ne = config.getint('Options','Ne') # Nsteps for sufficeint executed events.

        # Lists for rescaling barriers
        self.tgen = [] # times generated.
        self.us = [] # random deviates used
        self.ks = []# rate-constants used
        
        # Lists for tracking superbasin
        self.r_S = np.zeros(len(self.events)) #all rates in current superbasin
        self.dt_S = [] # dt used to compute rs in current superbasin
        
        
        # nem is the number of events performed in current superbasin.
        self.nem = np.zeros(len(self.events), dtype=int)
        # Nm is the number of events performed the last Nf steps.
        self.Nm = [np.zeros(self.ne,dtype=int) for e in self.events]
        
        self.Suffex = [] # suffiently executed quasi-equilibrated events

        # Variables for time and step-keeping
        self.isup = 0 # Superbasin step counter
        self.pm = 0
        # --------------------------------------------------
        
        # FRM method variables
        self.frm_times = [] # Needed later
        self.frm_arg = None # args that sort frm times (defined in frm_init())
        if self.verbose:
            print('Initializing First Reaction Method Lists...')
        self.frm_init()


    def frm_init(self):
        r"""Prepare to perform FRM simulation.
            
            Method initializes empty rate and event lists
            to bookkeep the FRM algorithm. The initial times
            of occurence for each event at each site is also
            calculated and stored.

        """
        self.rs = [] # (site & neighbor) -> rate 
        self.siteslist = [] # (site & neighbor) -> site  (site initial or si)
        self.evs = [] # (site & neighbor) -> event id
        self.other_sitelist = [] # (site & neighbor) -> other_site (site final or sf)
        self.lastsel = 0
        self.lastother = self.system.neighbors[0][0]
        self.rindex = [[[] for b in self.events] for a in self.system.sites]

        self.possible_evs = [] # used for superbasin.
        ks = []
        for i,lson in enumerate(self.system.neighbors): # i: site id, lson: list of neighbors
            for ie, e in enumerate(self.events): # ie: event id
                for j in lson: # j: a neighbor site id
                    if e.possible(self.system, i, j):
                        rcur = e.get_rate(self.system, i, j)
                        u = uniform(0.,1.)
                        self.frm_times.append( self.t - np.log(u) / rcur )
                        self.tgen.append(self.t)
                        self.us.append(u)
                        self.possible_evs.append(True)
                        
                    else:
                        rcur = 0.
                        self.frm_times.append(1E16) # 1e9 ??? IS IT INFINITY ???
                        self.tgen.append(self.t)
                        self.us.append(uniform(0.,1.))
                        self.possible_evs.append(False)
                    
                    #ks.append( e.get_rate(self.system, i, j) ) # SHALL WE USE rcur instead of calling get_rate() again?
                    ks.append( rcur ) # SHALL WE USE rcur instead of calling get_rate() again?
                    self.rindex[i][ie].append(len(self.rs))
                    self.evs.append(ie)            
                    self.rs.append(rcur)
                    self.siteslist.append(i)
                    self.other_sitelist.append(j)
        
        self.frm_times = np.array(self.frm_times)
        self.evs = np.array(self.evs)
        self.rs = np.array(self.rs)
        self.ks = np.array(ks)
        self.siteslist = np.array(self.siteslist)
        self.other_sitelist = np.array(self.other_sitelist)

        # event id -> list of site&neighbor where the event is possible 
        self.wheres = [np.where(self.evs==i) for i,e in enumerate(self.events)]
        
        # event id -> average rate
        self.ksavg = [np.mean([self.ks[i] for i in self.wheres[j][0]]) for j,e in enumerate(self.events)]
        self.frm_arg = self.frm_times.argmin() # site&neighbor of the most probable event


    def frm_step(self):
        r""" Takes a Monte Carlo Step.
        
            Method takes a Monte Carlo step by performing the next
            possible event, which has index 'self.frm_arg'in the 
            event list.

        """
        # Choose the first reaction if possible
        si = self.siteslist[self.frm_arg] # The site id to do event.
        sf = self.other_sitelist[self.frm_arg]
        self.lastsel = int(si)
        self.lastother = int(sf)

        ev_id = self.evs[self.frm_arg] 
        event = self.events[ev_id]

        site = self.system.sites[si]
        if event.possible(self.system, si, sf):
            self.current_event = event
            event.do_event(self.system,  si, sf) # Event is possible: change state

            self.t = self.frm_times[self.frm_arg] # Update time
            
            # Statistics
            self.evs_exec[ev_id] += 1
            self.stype_ev[site.stype][ev_id] += 1
            self.stype_ev_other[self.system.sites[sf].stype][ev_id] += 1

            # Find new enabled processes, update superbasin
            self.superbasin(ev_id, float(self.frm_times[self.frm_arg]-self.t))

            # .superbasin() calls .leave_superbasin(), which calls .rescaling()
                        
        else:
            # New first reaction must be determined
            raise Warning("Impossible event were next in queue and was attempted")

        # Save where the event happened:
        self.frm_update()
   

    def frm_update(self):
        r"""Updates the FRM related lists.
            
            Method updates the event list locally
            about the site where the last event happened
            by determining if new events have become
            possible due to performing the last event.

            This is done by keeping track of Nearest 
            neighbors and next nearest neighbors in
            'NNlast', 'NNNlast', 'NNother', and 'NNNother'.

        """
    
        # First find the site from the index:
        NNlast = self.system.neighbors[self.lastsel]
        NNother = self.system.neighbors[self.lastother]
        NNNlast = []; NNNother=[];
        for i in NNlast:
            NNNlast.extend(self.system.neighbors[i])
        
        for i in NNother:
            NNNother.extend(self.system.neighbors[i])

        search = [self.lastsel,self.lastother]
        search.extend(NNlast)
        search.extend(NNother)
        search.extend(NNNlast)
        search.extend(NNNother)
        search = list(set(search)) # Remove doubles # search is a list of site IDs

        # Save reference to function calls
        # To reduce overhead
        get_r_func = [e.get_rate for e in self.events]
        possible_func = [e.possible for e in self.events]

        for i in search: # Determine if any events have become possible.
            for j, e in enumerate(self.events):
                for k, other in enumerate(self.system.neighbors[i]):
                    poslist = self.rindex[i][j][k]
                    poss_now = possible_func[j](self.system, i, other)
                     # Only newly avaible events
                    if poss_now and self.possible_evs[poslist]==False:

                        rcur = get_r_func[j](self.system, i, other)
                        self.rs[poslist] = rcur
                        u = uniform(0.,1.)
                        self.frm_times[poslist] = self.t - np.log(u) / rcur
                        self.tgen[poslist] = self.t
                        self.us[poslist] = u
                        self.possible_evs[poslist] = True
                       
                    elif not poss_now:
                        self.rs[poslist] = 0.
                        self.frm_times[poslist] = 1E16
                        self.tgen[poslist] = 1E16
                        self.us[poslist] = uniform(0.,1.)
                        self.possible_evs[poslist] = False

        # New first reaction ?
        self.frm_arg = self.frm_times.argmin()

    def rescaling(self):
        """
        Rescales the times with alphas. Events that are now in the past are set
        as immediate events if still possible.
        """
        for ev in self.equilEV:
            i_up = np.where( self.evs==ev )[0]
            event = self.events[ev]
            if hasattr(event, 'possibles'):
                self.rescaling_batch(event, i_up)
            else:
                self.rescaling_loop(event, i_up)
                

    def rescaling_loop(self, event, i_up):
        """  Rescales the times with alphas one-by-one, using the methods 
        event.possible() and event.get_rate() """

        for i in i_up:
            site = self.siteslist[i] # The site to do event.
            othersite = self.other_sitelist[i]

            if event.possible(self.system, site, othersite):
                self.rs[i] = event.get_rate(self.system, site, othersite)
                try:
                    u0 = -np.log(self.us[i])/self.rs[i]
                    if self.t < self.tgen[i]+u0:
                        self.frm_times[i] =  self.tgen[i]+u0
                    else:
                        self.us[i] = uniform(0.0,1.0)
                        self.tgen[i] = self.t
                        self.frm_times[i] = self.t-\
                                   np.log(self.us[i])/self.rs[i]
                except:
                    self.frm_times[i] = 1E16

    def rescaling_batch(self, event, i_up):
        """  Rescales the times with alphas blockwise, using the methods 
        event.possibles() and event.get_rates() """

        sites = self.siteslist[i_up] # Sites to do event.
        other_sites = self.other_sitelist[i_up] # Neighbor sites to do event.
        possibles = event.possibles(self.system, (sites, other_sites) )

        for i in np.where( possibles )[0]:
            self.rs[i] = event.get_rate(self.system, sites[i], other_sites[i])
            try:
                u0 = -np.log(self.us[i]) / self.rs[i]
                if self.t < self.tgen[i] + u0:
                    self.frm_times[i] = self.tgen[i] + u0
                else:
                    self.us[i] = uniform(0.0,1.0)
                    self.tgen[i] = self.t
                    self.frm_times[i] = self.t - np.log(self.us[i])/self.rs[i]
            except:
                self.frm_times[i] = 1E16

    def leave_superbasin(self):
        """
        Resets all rate-scalings and
        connected statistics connected to
        the superbasin.
        """

        for e in self.equilEV:
            self.events[e].alpha = 1.
        self.rescaling()
        self.Suffex = []
        self.r_S = np.zeros(len(self.events))
        self.dt_S = []
        self.nem = np.zeros(len(self.events),dtype=int)
        self.isup =0


    def superbasin(self, evtype, dt):
        """
        Keeps track and performs barrier adjustments,
        of the generalized temporal acceleration scheme
        (DOI: 10.1021/acs.jctc.6b00859)
        """
        # Update the rates in the current superbasin
        #dtsup = dt
        if dt < 0:
            raise Warning("Time-step is < 0. Quitting")
        farg = int(self.frm_arg) 
        self.pm = (self.pm+1) %  self.ne
        self.nem[evtype] += 1.
        self.Nm[evtype][self.pm] = 1.

        self.r_S += [(self.rs*dt)[self.wheres[i][0]].sum() for i in range(len(self.events))] 
        self.dt_S.append(dt)
 
        # See if event is quasi-equilibrated
        if evtype in self.reverses:
            rev = abs(self.Nm[evtype].sum()-\
                      self.Nm[self.reverses[evtype]].sum())

            Nexm = self.Nm[evtype].sum()+\
                        self.Nm[self.reverses[evtype]].sum()

            if evtype not in self.equilEV:
                if Nexm >= self.ne/2. and rev < self.delta*self.ne:
                    self.equilEV.append(evtype)
                    if evtype != self.reverses[evtype]:
                        self.equilEV.append(self.reverses[evtype])
            
                else:
                    self.leave_superbasin()


            if evtype in self.equilEV and self.nem[evtype]+\
                        self.nem[self.reverses[evtype]] >= self.ne \
                        and evtype in self.equilEV\
                        and evtype not in self.Suffex: 

                self.Suffex.append(evtype)

                if evtype != self.reverses[evtype]:
                    self.Suffex.append(self.reverses[evtype])

        else: # Not reversible
            self.leave_superbasin()        

        if self.isup > self.Ns: # If observation perioud is over, scale events.
            r_S = 0.
            dtS = sum(self.dt_S)
            E = [i for i in range(len(self.events)) if i not in self.Suffex]

            r_S = max([self.ksavg[neqev] for neqev in E]) 
            
            for ev in [e for e in self.equilEV if e in self.Suffex]:
                rmev = self.r_S[ev]/dtS
                rmrev = self.r_S[self.reverses[ev]]/dtS
                alpham = min(self.Nf*r_S/(rmev+rmrev),1)
                self.events[ev].alpha *= alpham
           
            self.rescaling()
            
            self.isup = 0

        self.isup += 1
        
 
    def __str__(self):
        return "NeighborKMCBase: len(events) = {}, \
parameters = {}, system = {} ".format(
            len(self.events), self.parameters, self.system)


    def save_pickle(self,filename='transitions'):
        r""" Saves a pickle file with the simulation data.
        
             Method saves the number of events executed on
             the different types of sites, the time vs mcstep,
             the site-types, and optionally the coverages if
             'self.covered' is True.

             Last lists are cleaned from memory.

        """
        f = open(filename+'.pickle','wb')
        if self.verbose:
            print('Saving ', filename+'.pickle', '...')

        out = {'time':self.times,'nevents':self.evs_exec, 
                    'siteids':[m.ind for m in self.system.sites],
                    'stypes':[m.stype for m in self.system.sites],
                    'stype_ev':self.stype_ev,
                    'stype_ev_other':self.stype_ev_other,
                    'Nsites':self.Nsites,'mcstep':self.MCstep,
                    'tend':self.tend,'parameters':self.parameters}

        if self.save_coverages is True:
            out['covered'] = self.covered
            with open("coverages.txt","a") as f2:
                np.savetxt(f2,self.covered)

        pickle.dump(out,f)
        f.close()
        # Save additional txt files:
        with open("stype_ev.txt","ab") as f2:
            np.savetxt(f2,list(self.stype_ev.values()))

        with open("stype_ev_other.txt","ab") as f2:
            np.savetxt(f2,list(self.stype_ev_other.values()))
 
        with open("time.txt","ab") as f2:
            np.savetxt(f2,self.times)

        # Clear up arrays that grow with time:
        self.times = []
        evnl = [0 for i in range(len(self.events))]
    
        self.covered = []


    def get_coverages(self):
        r"""Gets the coverages at the present moment.

           Returns
           -------
           A list of coverages for each species and all sites
           Thus to find the coverage of species i on site 
           number j : ret[i][j]

        """
        ret = []
        for sp_id in range(self.Nspecies+1):
            cspec = [self.system.sites[i].covered for i\
                    in range(self.Nsites) if\
                    self.system.sites[i].covered == sp_id]

            ret.append(float(len(cspec))/float(self.Nsites))
        
        return ret

    def write_atoms(self,filename):
        r""" Write self.atom_cfgs to file with filename(string)"""
        
        return write(filename,images=self.atom_cfgs)

        
    def cover_system(self,species,coverage):
        r"""Covers the system with a certain species.
            
            Method covers the system with a species 'species', at a 
            certain coverage 'coverage'.
    
            Parameters
            ----------
            species : int
                The species as defined by hte user (e.g. empty=0,CO=1)

            coverage  : float
                The fractional coverage to load lattice with.

        """
        n_covered = int(np.round(coverage*len(self.system.sites)))
        chosen_sites = np.random.choice(len(self.system.sites),n_covered)
        for c in chosen_sites:
            self.system.sites[c].covered = species
        

    def load_events(self):
        raise NotImplementedError(r"""User needs to define load_events
                                 method in derived NeighborKMC class""")


    def set_tags(self):
        raise NotImplementedError(r"""User needs to define set_tags 
                                  method in derived NeighborKMC class""")

    def run_kmc(self):
        raise NotImplementedError(r"""User needs to define run_kmc method 
                                          in derived NeighborKMC class""")





