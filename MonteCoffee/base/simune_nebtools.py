import numpy as np

from ase.neb import NEBTools

from ase.calculators.emt import EMT

class SimuneNEBTools(NEBTools):
    """ Count number of maxima in the barrier and a better plotting """

    def __init__(self, all_images, backup_calculator=EMT()):
        """ 
        all_images: list of ASE images generated during the NEB optimization
        backup_calculator: calculator to use in case the energy is not defined
        """
        self.all_images = all_images
        for nneb, image in enumerate(all_images[1:]):
            if image==all_images[0]: break
        nneb +=1
        NEBTools.__init__(self, all_images[-nneb:])


        if backup_calculator is None: # Nothing more to do if backup_calculator is not defined...
            return
            
        for i in self._images:
            try:
                i.get_potential_energy()
            except:
                i.set_calculator(backup_calculator)
                i.get_potential_energy()
                

    def get_nmaxima(self):
        """ Count number of maxima """
        ee = [image.get_potential_energy() for image in self._images]
        return sum(np.diff(np.sign(np.diff(ee)))<0)

    def get_argmax(self):
        """ Locate absolute maximum """
        ee = [image.get_potential_energy() for image in self._images]
        return np.argmax(ee)

    def plot_band(self, ax=None):
        """Plots the NEB band on matplotlib axes object 'ax'. If ax=None
        returns a new figure object."""
        if ax is None:
            import matplotlib.pyplot as plt
            ax = plt.gca()

        s, E, Sfit, Efit, lines = self.get_fit()

         
        ax.plot(s, E, 'o', color='grey')
        
    
        for x, y in lines:
            ax.plot(x, y, '-g')
        
        ax.plot(Sfit, Efit, 'k-')

        ax.plot(s[0], E[0], 'o', color='red')
        argmax = np.argmax(E)
        ax.plot(s[argmax], E[argmax], 'o', color='green')
        ax.plot(s[-1], E[-1], 'o', color='blue')

        ax.set_xlabel(r'Path ($\AA$)')
        ax.set_ylabel('Energy (eV)')
        Ef = max(Efit) - E[0]
        Er = max(Efit) - E[-1]
        dE = E[-1] - E[0]
        
        ax.set_title("$E_\\mathrm{{a}} \\approx$ {:.3f} eV; "
                 "$E_\\mathrm{{r}} \\approx$ {:.3f} eV; "
                 "$\\Delta E$ = {:.3f} eV".format(Ef, Er, dE))
        
        return ax
