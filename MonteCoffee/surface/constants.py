r"""
Module: user_constants.py

Contains physical and user-defined constants.

These constants can be used together with
user_energy.py, user_entropy.py, and user_events.py

"""
K2Ha = 0.00000316679085237 # Kelvin2Hartree conversion factor 

