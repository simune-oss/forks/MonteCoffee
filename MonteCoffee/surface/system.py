r"""
Module: surface/system.py

The system module defines the SystemSurface Class, which is
a derived class from the SystemBase class (in the base.system module)

"""

import sys
import numpy as np
from MonteCoffee.base.system import SystemBase

class SystemSurface(SystemBase):
    

    def __init__(self, atoms, sites=[], adsorbates=[None,'Ag'], **kw):
        
        r"""Constructor for System objects.
            
            Method calls the base class system.py constructor, 
            defines a nearest neighbor cutoof distance, and 
            calculates the neighborlist.

    
            Parameters
            ----------
            atoms : ASE.Atoms should be passed in this example.
            sites : list of sites that constitute the system.

            Returns
            -------
            System instance

            See Also
            --------
            The module base.system

        """
        
        SystemBase.__init__(self, atoms=atoms, sites=sites)

        assert isinstance(adsorbates, list) # This is for representation purpose...
        self.adsorbates = adsorbates # List of adsorbate or diffusion species (instances of ASE's Atoms class, or strings for atomic adsorbates)

        coverage = kw['coverage'] if 'coverage' in kw else None
        natoms_ads = kw['natoms_ads'] if 'natoms_ads' in kw else None
        self.cover_system(coverage=coverage, natoms_ads=natoms_ads)

        self.ij2sid = ij2s = self.get_ij2sid()
        self.sid2ij = self.invert_2d_1d(self.ij2sid)
        self.ij2occ = np.zeros_like(self.ij2sid) # (i,j) -> adsorbate ID
        self.sid2occ = np.zeros( len(sites), dtype=int ) # (sid) -> adsorbate ID
        self.update_ij2occ()
        self.shape, self.ndim, self.size = ij2s.shape, ij2s.ndim, ij2s.size
        self.identify_neighbors()
        self.ash01 = np.array([[-self.shape[0], 0, +self.shape[0]],
                               [-self.shape[1], 0, +self.shape[1]]], dtype=int) # Convenience for the method .sqr_dist_pbc()
        

    def get_ij2sid(self, decimals=5):
        """ Tries to find a 2D lattice and the correspondence between
        the lattice's knotes (i,j) and sites' IDs (sid). """ 
        scal_pos = self.atoms.get_scaled_positions()
        site2pos = np.array([scal_pos[s.ind[0],:] for s in self.sites])
        rsp0 = np.unique(site2pos[:,0].round(decimals=decimals))
        rsp1 = np.unique(site2pos[:,1].round(decimals=decimals))
        m, n = len( rsp0 ), len( rsp1 ) # Sizes of the slab
        assert m*n == len(self.sites)
        ij2sid = np.zeros((m,n), dtype=int)

        for s, r in enumerate(site2pos.round(decimals=decimals)):
            i,j = np.argmin(abs(r[0]-rsp0)), np.argmin(abs(r[1]-rsp1))
            ij2sid[i,j] = s
        return ij2sid


    def invert_2d_1d(self, ij2sid):
        """ Inverts the 2d->1d correspondence: SOME BETTER solution ? """ 
        sid2ij = np.zeros( (ij2sid.size, 2), dtype=int)
        for i in range(ij2sid.shape[0]):
            for j in range(ij2sid.shape[1]):
                sid2ij[ij2sid[i,j], :] = (i,j)
        return sid2ij
    
    def update_ij2occ(self):
        """ Synchronizes the arrays ij2aid[:,:] and ij2aid[:,:] with
            the sites' occupations globally."""
        for i, v in enumerate(self.ij2sid):
            for j, sid in enumerate(v):
                self.ij2occ[i,j] = self.sid2occ[sid] = self.sites[sid].covered

    def update_ij2occ_local(self, si, sf):
        """ Synchronizes the arrays ij2aid[:,:] and sid2aid[] array with the
        sites' occupations after an event, (i.e. for two sites only)"""
        self.ij2occ[tuple(self.sid2ij[si])] = self.sid2occ[si] = self.sites[si].covered
        self.ij2occ[tuple(self.sid2ij[sf])] = self.sid2occ[sf] = self.sites[sf].covered

    def check_ij2occ_and_sid2occ(self):
        """ """
        for i, v in enumerate(self.ij2sid):
            for j, sid in enumerate(v):
                if self.ij2occ[i,j] != self.sid2occ[sid]:
                    return 2
                if self.ij2occ[i,j] != self.sites[sid].covered:
                    return 1
        return 0

    def get_ase_atoms(self, atoms=None, sites=None, dh=0.0, rm_sub=False):
        r""" Atoms object with current atomic configuration 
          atoms : if necessary 
          sites : external sites list
          dh    : adjustments of the height
          rm_sub: remove the substrate atoms 
        Returns :
          ASE's Atoms object
          
        """
        import ase.build
        atoms = self.atoms.copy() if atoms is None else atoms.copy()
        lensubstrate = len(atoms)
        sites = self.sites if sites is None else sites
        for sid, site in enumerate(sites):
            if self.adsorbates[site.covered] is not None:
                pos = atoms[site.ind[0]].position
                ase.build.add_adsorbate(atoms, self.adsorbates[site.covered],
                   height=1.8845+dh, position=(pos[0],pos[1]))
        
        if rm_sub:
            del atoms[list(range(lensubstrate))]
            
        return atoms

    def get_adsorbate_atom_id(self, refsid, atoms=None, sites=None):
        r""" Atoms object with current atomic configuration 
          refsid: site id for which the atom index of the adsorbate atom is needed
          atoms : if necessary 
          sites : external sites list
        Returns :
          atom ID of the adsorbate atom
          
        """
        import ase.build
        
        atom_id = None
        atoms = self.atoms.copy() if atoms is None else atoms.copy()
        sites = self.sites if sites is None else sites
        for sid, site in enumerate(sites):
            if self.adsorbates[site.covered] is not None:
                pos = atoms[site.ind[0]].position
                ase.build.add_adsorbate(atoms, self.adsorbates[site.covered],
                   height=1.8845, position=(pos[0],pos[1]))
                if sid==refsid: 
                    atom_id = len(atoms)-1
                    
        return atom_id

    def identify_neighbors(self):
        r"""The neighbors are identified, based on the first nearest-neighbor
        connectivity on a rectangular periodic lattice.
        """
        for sid, ij in enumerate(self.sid2ij):
            s = self.sites[sid]
            s.neighbors = []
            for nn in ([1,0], [0,1], [-1,0], [0,-1]):
                k,l = np.remainder(ij+nn, self.shape)
                s.neighbors.append(self.ij2sid[k,l])
            self.neighbors[sid] = s.neighbors

    def get_cn2nsites(self):
        """ Compute a coordination number histogram: 
        Coordination number --> number of sites with that coordination number.
        """
        cn2n = np.zeros(5)
        sid2cov = np.array([s.covered for s in self.sites])
        ls2sid  = np.where(sid2cov>0)
        for i in ls2sid[0]:
            cn2n[ sid2cov[self.neighbors[i]].sum() ] +=1

        return cn2n
        
    def cover_single_site(self, at=None, specie=1):
        """ The method adds an adsorbate atoms to a given position on the lattice """
        
        sid = self.ij2sid[at]
        self.sites[sid].covered = specie
        self.update_ij2occ()
        return sid
    
    def get_nads(self):
        """ Count the number of adsorbate atoms """
        nads_sites = 0
        for s in self.sites: 
            if s.covered>0: nads_sites += 1
        
        nads_latt = (self.ij2occ>0).sum()
        return nads_sites, nads_latt

    def sqr_dist_pbc(self, xx):
        """ Get Lattice-periodic distance between sites given by their coordinates 
            xx : list of coordinates of two sites 
        """
        z = np.array(xx).T
        d2 = (np.amin(np.absolute((z[1]-z[0]).reshape(2,1)-self.ash01), axis=1)**2).sum()
        return d2
